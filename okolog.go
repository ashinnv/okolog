package okolog

import "fmt"

func ErrorSendoff(logTarget string, message string, err error) {
	fmt.Errorf("Not sending %e to %s with message: %s because network logging is not implemented yet.", err, logTarget, message)
}
